var xoff;
var inc = 0.1;
var scl = 10;
var cols, rows;
var zoff = 0;
var z = 0;

var octaves = 8;
var falloff = 0.5;
var noiseScale = 0.02;
var pixelScale = 6;


//PARTICLE
var particles = [];
var flowfield;

function setup()
{
    createCanvas(512,512);
    
    colorMode(RGB, 255);
    //pixelDensity(1);
    
    cols = floor(width / scl);
    rows = floor(height / scl);
    
    //array for the flowfield of the particles
    flowfield = new Array(cols * rows);
    for(var i = 0; i < 100; i++)
    {
        //storing random values for the location of the line in the array 
        particles[i] = new Particle();
    }
    background(255);
}

//NOISE DETAIL AND FLOW FIELD - 3D
function draw()
{
    //background(0);
    
    //loop which connects the particles of the flow field and outputs a thready-hair like drawing.
    var xoff = 0;
    for(var x = 0; x < rows; x++)
    {
        var yoff = 0;
        for( var y = 0; y < cols; y++)
        {
            var index = x + y * cols;
            var angle = noise(xoff, yoff, zoff) * TWO_PI * 5; 
            //var v = p5.Vector.fromAngle(noiseVal);
            var v = p5.Vector.fromAngle(angle);
            v.setMag(1);
            flowfield[index] = v; 
            yoff += inc;
            stroke(0, 50);
        }
        xoff += inc;
        zoff += 0.0001;
    }
    
    for(var i = 0; i < particles.length; i++)
    {
        particles[i].follow(flowfield);
        particles[i].update();
        particles[i].edges();
        particles[i].show();
    }
    
    //dividing th screen into four different quadrants and displaying dots according to that 
    for (var x=0; x < 10; x++) 
    {
        for (var y=0; y < 10; y++) 
        {
            //storing random value from 4 different quadrants and using that to display the dots
            var quad1_x = random(0 ,width/3);
            var quad1_y = random(0, height/3);
            
            var quad2_x = random(width/2, width);
            var quad2_y = random(0, height/2);

            var quad3_x = random(0, width/2);
            var quad3_y = random(height/2, height);

            var quad4_x = random(width/2, width);
            var quad4_y = random(height/2, height);
            
            var noiseVal = noise(x * noiseScale, y * noiseScale);
            
            //the ocean - diffrent colors depict different quadrants.
            stroke(255, 128, 114, 25);
            point(quad1_x, quad1_y);
            rotate(noiseVal++);
            
            stroke(5, 150, 204, 80);
            point(quad4_x, quad4_y);
            rotate(noiseVal++);
            
            stroke(255, 255, 255, 20);
            point(quad2_x, quad2_y);
            rotate(noiseVal++);
            
            stroke(5, 150, 170, 50);
            point(quad3_x, quad3_y);
            rotate(noiseVal++);
          }
    }
    
}

// function which helps the display of the flow fiels
function Particle()
{
    //position
    var pos = createVector(random(width), random(height));
    //velocity
    var vel = createVector(0,0);
    //acceleration
    var acc = createVector(0,0);
    var maxspeed = 4; //speed at which the line is drawn
    var r = 0; 
    var prevPos = pos.copy();
    
    this.update = function() {
        vel.add(acc);
        vel.limit(maxspeed);
        pos.add(vel);
        acc.mult(0);
    }
    
    this.follow = function(vectors) {
        var x = floor(pos.x / scl);
        var y = floor(pos.y / scl);
        var index = x + y * cols;
        var force = vectors[index];
        this.applyForce(force);
    }
        
    this.applyForce = function(force) {
        acc.add(force);
    }
    
    //function for the constant change in color and for adjusting the stroke Weight and display of the line
    this.show = function() {
        stroke(0,0,255);
        r = r + 1;
        if(r > 255){
            r = 0;
        }
        strokeWeight(1);
        //point(pos.x, pos.y);
        line(pos.x, pos.y, prevPos.x, prevPos.y);
        
        this.updatePrev();
    }
    
    this.updatePrev = function() {
        prevPos.x = pos.x;
        prevPos.y = pos.y;
    }
        
    //function to make sure that all the waves are wrapped around
    this.edges = function() {
        if (pos.x > width)
        {
            pos.x = 0;
            this.updatePrev();
        }
        if (pos.x < 0)
        {
            pos.x = width;
            this.updatePrev();
        }
        if (pos.y > height)
        {
            pos.y = 0;
            this.updatePrev();
        }
        if (pos.y < 0)
        {
            pos.y = height;
            this.updatePrev();
        }
    }
}

